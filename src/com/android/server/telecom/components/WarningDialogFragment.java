/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.telecom.components;

import android.telecom.Log;
import com.android.server.telecom.R;
import com.android.server.telecom.Call;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Used to display an error dialog from within the Telecom service when an outgoing call fails
 */
public class WarningDialogFragment extends DialogFragment {
    private static final String TAG = "WarningDialogFragment";

    public static final int WARNING_TYPE_NONE = 0;
    public static final int WARNING_TYPE_ROAMING = 1;
    public static final int WARNING_TYPE_WPS = 2;

    private boolean mIsSelected = false;
    private int mWarningType;
    private int mMsgResId = R.string.international_roaming_warning;
    private SelectWarningListener mListener;

    public WarningDialogFragment() {}

    public static WarningDialogFragment getInstance(
        int msgResId,
        int warningType,
        SelectWarningListener listener) {
        WarningDialogFragment fragment = new WarningDialogFragment();
        fragment.setMessageId(msgResId);
        fragment.setWarningType(warningType);
        fragment.setListener(listener);
        return fragment;
    }

    public void setMessageId(int msgResId) {
        mMsgResId = msgResId;
    }

    public void setWarningType(int warningType) {
        mWarningType = warningType;
    }

    public void setListener(SelectWarningListener listener) {
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mIsSelected = false;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = null;

        if (mWarningType == WARNING_TYPE_ROAMING) {
            builder.setTitle(R.string.roaming_warning_dialog_title);
            builder.setMessage(mMsgResId);
            builder.setPositiveButton(R.string.modify_roaming_setting,
                                   new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(this, "Click Abort call on roaming");
                    mIsSelected = true;
                    onDialogClicked(false);
                }
            });
            builder.setNegativeButton(R.string.continue_roaming_setting,
                                   new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(this, "Click Continue call on roaming");
                    mIsSelected = true;
                    onDialogClicked(true);
                }
            });
        } else if (mWarningType == WARNING_TYPE_WPS) {
            final String items[] = {"Continue the WPS call",
                                    "Abort the WPS call"};
            builder.setTitle("WPS Call. Choose option");
            builder.setCancelable(true);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    mIsSelected = true;
                    if (item == 1) {
                        // abort WPS call
                        onDialogClicked(false);
                    } else {
                        // continue WPS call
                        onDialogClicked(true);
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    mIsSelected = true;
                    Log.d(this, "Click cancel");
                    // abort WPS call
                    onDialogClicked(false);
                }
             });
        }
        dialog = builder.create();
        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
      Log.d(this, "onCancel mIsSelected: " + mIsSelected);
      if (!mIsSelected && mListener != null) {
          Bundle result = new Bundle();
          result.putInt(SelectWarningListener.EXTRA_TYPE, mWarningType);
          mListener.onReceiveResult(SelectWarningListener.RESULT_DISMISSED, result);
      }
      super.onCancel(dialog);
    }

    private void onDialogClicked(boolean isContinue) {
        Bundle result = new Bundle();
        result.putBoolean(SelectWarningListener.EXTRA_CONTINUE, isContinue);
        result.putInt(SelectWarningListener.EXTRA_TYPE, mWarningType);
        if (mListener != null) {
            mListener.onReceiveResult(SelectWarningListener.RESULT_SELECTED, result);
        }
    }

    public static class SelectWarningListener extends ResultReceiver {
        static final int RESULT_SELECTED = 1;
        static final int RESULT_DISMISSED = 2;

        static final String EXTRA_CONTINUE = "extra_continue";
        static final String EXTRA_TYPE = "extra_type";

        protected SelectWarningListener() {
            super(new Handler());
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == RESULT_SELECTED) {
                onSelectDialog(
                    resultData.getInt(EXTRA_TYPE),
                    resultData.getBoolean(EXTRA_CONTINUE));
            } else if (resultCode == RESULT_DISMISSED) {
                onDialogDismissed(resultData.getInt(EXTRA_TYPE));
            }
        }

        public void onSelectDialog(int warningType, boolean isContinue) {}
        public void onDialogDismissed(int warningType) {}
    }
}
