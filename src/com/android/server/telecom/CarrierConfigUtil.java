package com.android.server.telecom;

import android.content.Context;
import android.os.PersistableBundle;
import android.telecom.Log;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;

public class CarrierConfigUtil {
    private final static String TAG = "CarrierConfigUtil";

    private static int getDefaultSubscriptionId(Context context) {
        return SubscriptionManager.from(context).getDefaultSubscriptionId();
    }

    public static boolean getBooleanCarrierConfig(Context context, String key) {
        return getBooleanCarrierConfig(context, key, getDefaultSubscriptionId(context));
    }

    public static boolean getBooleanCarrierConfig(Context context, String key, int subId) {
        boolean ret = false;
        PersistableBundle b = null;
        CarrierConfigManager cfgManager = (CarrierConfigManager)
               context.getSystemService(Context.CARRIER_CONFIG_SERVICE);
        subId = subId > SubscriptionManager.INVALID_SUBSCRIPTION_ID ? subId : getDefaultSubscriptionId(context);

        if (cfgManager != null) {
            b = cfgManager.getConfigForSubId(subId);
        }

        if (b != null) {
            ret = b.getBoolean(key);
        } else {
            ret = CarrierConfigManager.getDefaultConfig().getBoolean(key);
        }
        Log.d(TAG, "getBooleanCarrierConfig - key: " + key + " ret: " + ret);
        return ret;
    }

    public static int getIntCarrierConfig(Context context, String key) {
        return getIntCarrierConfig(context, key, getDefaultSubscriptionId(context));
    }

    public static int getIntCarrierConfig(Context context, String key, int subId) {
        int ret = 0;
        PersistableBundle b = null;
        CarrierConfigManager cfgManager = (CarrierConfigManager)
               context.getSystemService(Context.CARRIER_CONFIG_SERVICE);

        subId = subId > SubscriptionManager.INVALID_SUBSCRIPTION_ID ? subId : getDefaultSubscriptionId(context);

        if (cfgManager != null) {
            b = cfgManager.getConfigForSubId(subId);
        }
        if (b != null) {
            ret = b.getInt(key);
        } else {
            ret = CarrierConfigManager.getDefaultConfig().getInt(key);
        }
        Log.d(TAG, "getBooleanCarrierConfig - key: " + key + " ret: " + ret);
        return ret;
    }
}
